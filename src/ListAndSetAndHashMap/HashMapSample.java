package ListAndSetAndHashMap;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class HashMapSample {
    public static void main(String[] args) {
        //در بخش همزمانی hashmap  مشکل دارد
        //  یعنی اینکه چند نفر همزمان از این لیست مقدار بخواهند
        //مقادیر تکراری را جایگزین میکند
        Map map = new HashMap();
        map.put("name", "javid");
        map.put("family", "sattar");
        map.put("age", 25);

        // برای جلوگیری از مشکل همزمانی از ConcurrentHashMap استفاده میشود
        Map cmap = new ConcurrentHashMap();
        cmap.put("name", "javid");
        cmap.put("family", "sattar");
        cmap.put("age", 25);

        System.out.println(map.get("name"));
        System.out.println(map.get("family"));
        System.out.println(map.get("age"));


        // میتوانیم hashMap را به صورت Generic مقدار دهی کنیم بصورت زیر
        Map<String, Integer> map1 = new ConcurrentHashMap<>();
        map1.put("age", 25);
        map1.put("may", 190);
        map1.put("weight", 75);
        System.out.println(map1.get("age"));
        System.out.println(map1.get("may"));
        System.out.println(map1.get("weight"));

        //برای مشاهده لیست کلید ها میتوان روی hashMap یک foreach زد بصورت زیر
        // برای زدن سریع foreach روی یک لیست میتوان iter را تایپ کرد
        Set set = map.keySet();
        for (Object o : set) {
            System.out.println("your key is: " + o + " Value : " + map.get(o));
        }

    }
}
