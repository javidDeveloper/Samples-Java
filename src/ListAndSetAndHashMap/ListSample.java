package ListAndSetAndHashMap;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ListSample {
    public static void main(String[] args) {
        List<String> list = new ArrayList();
        list.add("javid");
        list.add("sattar");
        list.add("JAVA");
        list.add("*************");
        System.out.println(list.get(0));
        System.out.println(list.get(1));
        System.out.println(list.get(2));
        System.out.println(list.get(3));

//        list.clear();   // for clear list
//        list.isEmpty();   // for Empty or full  list with true or false
//        list.remove(4);//for delete item from list
//        list.contains("JAVA"); // if this item in your list found return True
//        list.set(2,"Developer");// for add value to list by index

        //for get list value Sample_1
            int i = 0;
            while (list.size() != i) {
                System.out.println(list.get(i));
                i++;
            }
        //for get list value Sample_2
            Iterator iterator = list.iterator();
            while (iterator.hasNext())
            {System.out.println(iterator.next());
        }

        //for get list value Sample_3
        for (Object o:list) {
            System.out.println(o);
        }

        //for get list value Sample_4
        // ADD on Java 8 functional Programming
        list.stream().forEach(System.out::println);


        //for get list value Sample_5
        // ADD on Java 8 functional Programming
        list.stream().forEach((o)-> System.out.println(o));

    }
}
