package ListAndSetAndHashMap;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetSample {
    public static void main(String[] args) {

        // مقدار تکراری قبول نمیکند
        // HashSet پدرSet
        // ترتیب در set نگهداری نمیشود

        Set set = new HashSet();
        set.add("javid");
        set.add("javid");//تکراری نمیپذیرد
        set.add("sattar");
        set.add("JAVA");
        set.add("*************");
        for (Object o:set) {
            System.out.println(o);
        }

        Set tset = new TreeSet();// ترتیب را رعایت میکند
        tset.add("javid");
        tset.add("javid");//تکراری نمیپذیرد
        tset.add("sattar");
        tset.add("JAVA");
        tset.add("*************");
        for (Object o:tset) {
            System.out.println(o);
        }


    }
}
