package AnnotationSamples.RunCodeAnnotation;

@Annotation(Developer = "sattar", Date = 1399, Comment = "ClassMain")
public class main {
    public static void main(String[] args) {
        //  درکلاس Annotation برای اسنفاده از مقادیر Annotation باید مقدار
        // @Retention(RetentionPolicy.RUNTIME)
        // تعریف شود


        ClassB b = new ClassB();
        java.lang.annotation.Annotation[] annotation1 = b.getClass().getAnnotations();
        Annotation classBAn1 = (Annotation)annotation1[0];
        TestAnnotation classBAn2 = (TestAnnotation)annotation1[1];
        System.out.println("**********ClassB Annotation_1**********");
        System.out.println(classBAn1.Developer());
        System.out.println(classBAn1.Comment());
        System.out.println(classBAn1.Date());
        System.out.println("**********ClassB Annotation_2**********");
        System.out.println(classBAn2.owner());
        System.out.println("********************");


        ClassA a = new ClassA();
        Annotation annotation = a.getClass().getAnnotation(Annotation.class);
        System.out.println("**********ClassA**********");
        System.out.println(annotation.Developer());
        System.out.println(annotation.Comment());
        System.out.println(annotation.Date());
        System.out.println("********************");


        main main = new main();
        Annotation annotation3 = main.getClass().getAnnotation(Annotation.class);
        System.out.println("**********main**********");
        System.out.println(annotation3.Developer());
        System.out.println(annotation3.Comment());
        System.out.println(annotation3.Date());
        System.out.println("********************");

    }
}
