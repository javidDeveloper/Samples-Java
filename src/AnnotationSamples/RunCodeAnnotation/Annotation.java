package AnnotationSamples.RunCodeAnnotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Annotation {
    String Developer() default "javid";

    long Date();

    String Comment() default "NONE";
}
