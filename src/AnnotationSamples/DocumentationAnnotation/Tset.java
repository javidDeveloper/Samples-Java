package AnnotationSamples.DocumentationAnnotation;

public @interface Tset {
    String Developer() default "javid";

    long Date();

    String Comment() default "NONE";
}
