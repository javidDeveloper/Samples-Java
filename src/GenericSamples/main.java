package GenericSamples;

public class main {
    public static void main(String[] args) {
        // چون مقادیر را مشخص نکردیم مقدار دهی آزاد است
        Persion persion = new Persion();
        persion.x="test";
        persion.y= 1.025;


        //مقدار دهی باید بصورتی باشد که x  را از نوع Integer و y را از نوع String  قرار دهیم
        Persion<Integer,String> persion1 = new Persion<>();
        persion1.x=12;
        persion1.y="test";

        //مقدار دهی باید بصورتی باشد که x  را از نوع Boolean و y را از نوع Character  قرار دهیم

        Persion<Boolean,Character> persion2 = new Persion<>();
        persion2.x=true;
        persion2.y='a';
    }
}
