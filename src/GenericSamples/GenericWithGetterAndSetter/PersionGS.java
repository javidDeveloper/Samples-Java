package GenericSamples.GenericWithGetterAndSetter;

public class PersionGS<O1,O2,O3 extends Number> {
    private O1 x ;
    private O2 y ;
    private O3 z ;

    public O1 getX() {
        return x;
    }

    public void setX(O1 x) {
        this.x = x;
    }

    public O2 getY() {
        return y;
    }

    public void setY(O2 y) {
        this.y = y;
    }

    public O3 getZ() {
        return z;
    }

    public void setZ(O3 z) {
        this.z = z;
    }
}
