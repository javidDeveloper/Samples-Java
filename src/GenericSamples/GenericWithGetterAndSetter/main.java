package GenericSamples.GenericWithGetterAndSetter;

import EncapsollationAndGetterSetter.Persion;

public class main {
    public static void main(String[] args) {
        PersionGS gs = new PersionGS() ;
        gs.setX(12);
        gs.setY("salam");

        //مقدرا z حتما باید از نوع عددی باشد
        // چون در کلاس gs مقدار z محدود شده است
        gs.setZ(12.5);


        //مقدرا z حتما باید از نوع عددی باشد
        // چون در کلاس gs مقدار z محدود شده است
        PersionGS<Integer,String,Double> gs1 = new PersionGS<>();
        gs1.setX(1);
        gs1.setY("salam");
        gs1.setZ(123132132135646.0);
    }
}
