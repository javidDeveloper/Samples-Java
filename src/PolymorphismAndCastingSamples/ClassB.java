package PolymorphismAndCastingSamples;

public class ClassB extends ClassA {
    @Override
    public void name() {
        System.out.println("My name Is B");
    }

    public void family() {
        System.out.println("B family");

    }
}
