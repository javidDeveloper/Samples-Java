package PolymorphismAndCastingSamples;

public class main {
    public static void main(String[] args) {
        ClassA  a = new ClassB();// Polymorphism
        a.name();//print my name is B

        //in line Casting class A to class B
        ((ClassB) a).family();//print B family

        Object c = new ClassB();
        ((ClassB) c).name();
        ((ClassB) c).family();

    }
}
