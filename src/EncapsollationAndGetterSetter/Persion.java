package EncapsollationAndGetterSetter;

public class Persion {
    private String name;
    private String family;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.equals("bad"))
            this.name = "filter";
        else this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        if (family.equals("veryBad"))
            this.family = "filter";
        else this.family = family;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
