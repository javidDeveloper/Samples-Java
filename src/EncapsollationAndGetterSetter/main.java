package EncapsollationAndGetterSetter;

public class main {
    public static void main(String[] args) {
        Persion  persion =  new Persion();

//        persion.setName("bad");//print "filter"
//        persion.setFamily("veryBad");//print "filter"


        persion.setName("javid");//print "javid"
        persion.setFamily("sattar");//print "sattar"
        persion.setAge(22);

        System.out.println(persion.getName()+"\n"+
                persion.getFamily()+"\n"+persion.getAge());

    }

}
