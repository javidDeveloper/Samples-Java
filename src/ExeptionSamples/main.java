package ExeptionSamples;

import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            int x = Integer.parseInt(scanner.nextLine());
            int y = Integer.parseInt(scanner.nextLine());
            int z = Integer.parseInt(scanner.nextLine());
            if (x == 0 || y == 0 || z == 0)
                throw new ExeptionWrapper("zero not support");
            System.out.println(x / y / z);
        } catch (Exception e) {
            ExeptionWrapper.handler(e);
        }
    }
}
