package ExeptionSamples;

public class ExeptionWrapper extends Exception {
    public ExeptionWrapper(String msg) {
        super(msg);
    }
    public ExeptionWrapper( ) {
        super();
    }

    public static void handler(Exception e) {
        if (e instanceof ArithmeticException) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } else if (e instanceof NumberFormatException) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } else if (e instanceof ExeptionWrapper) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } else if (e instanceof Exception) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}
